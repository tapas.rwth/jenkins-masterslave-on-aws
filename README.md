Building Jenkins Master-Slave achitecture on AWS-EC2 linux instances. Detailed descriptions and methods are given in the separate word file 

* Launch 1 EC2 ubuntu 18.04 LTS server t2.micro instance
* install Jenkins on this server (this node will be Jenkins master)
* Launch 2 EC2 ubuntu18.04 LTS server t2.micro instances (these are used as slave nodes)
* Install Java and create a working directory in the slave nodes
* Connect 2 slave nodes using SSH Launch method from the Jenkins master node
